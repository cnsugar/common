# common

#### 项目介绍
Java项目开发用的公用基础模块，包括：Spring容器初始化、配置文件读取工具类、分页对象、Protobuf工具类、反射工具类等

#### 安装教程

1. 使用maven package打包成jar，引入到项目中；

#### 使用说明

1. 读取配置文件工具类的使用
  > SystemConfig继承了spring默认加载配置文件类org.springframework.beans.factory.config.PropertyPlaceholderConfigurer，并对其进行了增强，增加了一些常用的方法，所以只需要将原来配置文件中的PropertyPlaceholderConfigurer换成SystemConfig类即可，如下，在spring配置文件中加入bean定义:
  ```xml
  <bean class="com.cnsugar.common.config.SystemConfig">
      <property name="fileEncoding" value="UTF-8" />
      <property name="locations">
          <list>
              <value>classpath:application.properties</value>
          </list>
      </property>
   </bean>
  ```
   >在任何类中只需要用SystemConfig中的方法即可读取application.properties中的所有配置，如:
   ```java
   String str = SystemConfig.getString("key");
   int i = SystemConfig.getInt("key");
   ```

2. Spring容器工具类的使用

   > 通过该工具类，可以使普通的java应用程序集成spring来管理项目的各种对象。
   >
   > 在类中可以通过以下两种方式获取Spring容器中的所有bean对象:
   ```java
   UserService userService = AppContext.getBean("userService");
   UserService userService = AppContext.getBean(UserService.class);
   ```
   > 注意：如果在SpringMVC项目中使用该工具类，需要在listener初始化方法中调用AppContext.initWebApplicationContext()方法来同步SpringMVC容器，否会在日志中看到spring会初始化两次。如新建一个WebContextListener类：

   ```java
   public class WebContextListener extends org.springframework.web.context.ContextLoaderListener {
      @Override
      public WebApplicationContext initWebApplicationContext(ServletContext servletContext) {
         return super.initWebApplicationContext(servletContext);
      }

      @Override
      public void contextInitialized(ServletContextEvent event) {
         super.contextInitialized(event);
         initialize(event.getServletContext());
         StringBuilder sb = new StringBuilder();
         sb.append("\r\n======================================================================\r\n");
         sb.append("\r\n    欢迎使用 ").append(SystemConfig.getConfig("app.name")).append("\r\n");
         sb.append("\r\n    Version ").append(SystemConfig.getConfig("api.version")).append("\r\n");
         sb.append("\r\n======================================================================\r\n");
         System.out.println(sb.toString());
      }

      /**
       * 系统初始化
       * @param servletContext
       */
      private void initialize(ServletContext servletContext) {
         AppContext.initApplicationContext(WebApplicationContextUtils.getWebApplicationContext(servletContext));
         //初始化配置属性
         servletContext.setAttribute("conf", SystemConfig.getConfigMap());
      }

      @Override
      public void contextDestroyed(ServletContextEvent event) {
         super.contextDestroyed(event);
      }
   }
   ```

   > 在web.xml中加入：

   ```xml
   <listener>
      <listener-class>com.cnsugar.web.listener.WebContextListener</listener-class>
   </listener>
   ```

3. 反射工具类的使用

   > ReflectUtils工具类中的主要方法介绍：

   ```java
   /**
    * 获取类中非final和static的属性(包括父类中的属性)，带Field属性和对应的setter和getter方法Method对象
    */
   List<BeanField> getBeanFields(Class clazz);
   /**
    * 获取类或所有父类中非final和static的属性
    */
   List<Field> getFields(Class clazz);
   /**
    * 获取类或所有父类中setter方法
    */
   List<Method> getSetterMethods(Class clazz);
   /**
    * 获取类或所有父类中getter方法
    */
   List<Method> getGetterMethods(Class clazz);
   /**
    * 直接读取对象属性值, 无视private/protected修饰符, 不经过getter方法
    */
   Object getFieldValue(final Object obj, final String fieldName);
   /**
    * 直接设置对象属性值, 无视private/protected修饰符, 不经过setter方法
   */
   void setFieldValue(Object obj, String fieldName, Object value);
   ```

4. 分页Page对象的使用

   > 一般结合common-jdbc或基于mybatis的PageInterceptor使用，也可直接传一个数据List进去进行分页，toString()方法会生成基于bootstrap的分页操作栏的html代码，可直接在web页面显示。（后面会详细说明）。

5. 其他类说明

   > CamelCaseUtils: 驼峰命名法(CamelCase)和下划线风格(UnderScoreCase)字符串之间的转换工具类;
   >
   > ProtostuffUtil: protostuff工具类; 
   >
   > Utils: 包含常用的获取系统时间、日期转换、日期比较、时间格式化、普通java类型转换等方法工具类；
   >
   > Bytes: 各种java类型与byte[]互转工具类；
   >
   > Base64: Base64编码和解码工具类；

